package fr.bde_insa_lyon.billetteriereader;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity implements View.OnClickListener{

    private static final int RC_INTERNET_PERMISSION = 2;
    private boolean isValid = false;
    private boolean isSelected = false;
    private String API_URL ;
    private final MediaType mediaType = MediaType.parse("application/json");
    OkHttpClient client = new OkHttpClient();
    EditText username ;
    EditText password ;
    EditText file;
    TextView status;
    Button login;
    CheckBox remember;
    protected SharedPreferences settings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        setContentView(R.layout.activity_login);
        // if (MainActivity.DEVMODE) error_alert("Begin");

        final String[] permissions = new String[]{Manifest.permission.INTERNET};

        if(!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {
            ActivityCompat.requestPermissions(this, permissions, RC_INTERNET_PERMISSION);
            // if (MainActivity.DEVMODE) error_alert("Internet prompt");
        }

        // if (MainActivity.DEVMODE) error_alert("Clickable");
        username = (EditText) findViewById(R.id.userField);
        password = (EditText) findViewById(R.id.pwdField);
        file = (EditText) findViewById(R.id.posField);
        login = (Button) findViewById(R.id.loginBtn);
        status = (TextView) findViewById(R.id.textConnectStatus);
        remember = (CheckBox) findViewById(R.id.checkBox);
        remember.setChecked(settings.getBoolean("remember", false));
        if (settings.getBoolean("devmode", false)) {
            error_alert("Info","DEVMODE active");
            username.setText(R.string.orgaif);
            password.setText(settings.getString("devpwd", ""));
        }else{
            if (settings.getBoolean("remember", false)){
                username.setText(settings.getString("username",""));
                password.setText(settings.getString("password",""));
            }
        }
        API_URL = settings.getString("url", getString(R.string.api_url_prod));
        findViewById(R.id.loginBtn).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.assignBtn).setOnClickListener(this);
        remember.setOnClickListener(this);

    }

    @Override
    public void onClick (View view) {
        if(view.getId() == R.id.loginBtn){

            login.setClickable(false);

            String user = username.getText().toString();
            String pwd = password.getText().toString();

            String content = "{\n\t\"username\":\""+user+"\",\n\t\"password\":\""+pwd+"\"\n}";

            post(API_URL + "/api/authenticate", content, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("FAIL");
                    System.out.println(e);
                    login.setClickable(true);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    login.setClickable(true);
                    if(response.isSuccessful()) {
                        String responseStr = response.body().string();
                        JSONObject reader = null;
                        try {
                            reader = new JSONObject(responseStr);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("Error in JSON read");
                        }

                        try {
                            String token = reader.getString("token");
                            editSPref("token", token);
                            isValid = true;
                            if (settings.getBoolean("remember", false) && !settings.getBoolean("devmode", false)){
                                editSPref("username", username.getText().toString());
                                editSPref("password", password.getText().toString());
                            }else if (settings.getBoolean("remember", false)){
                                editSPref("devpwd", password.getText().toString());
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    status.setText(R.string.conn_not);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    error_alert("Error","Error while parsing JSON");
                                }
                            });
                        } catch (NullPointerException n) {
                            n.printStackTrace();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    error_alert("Error","Error on response. Is the service UP?");
                                }
                            });
                        }

                    } else {
                        // Request not successful
                        error_alert("ALERT", "Error during request");

                    }
                }
             });


        }

        if (view.getId() == R.id.assignBtn) {
            if(isValid) {
                status.setText(R.string.connected);
                int fil = Integer.parseInt(file.getText().toString());
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("file", fil);
                editor.apply();
                isSelected = true;
            }
        }

        if (view.getId() == R.id.back) {
            Intent resultIntent = new Intent();
            int state = Activity.RESULT_CANCELED;
            if (isValid && isSelected){
                state = Activity.RESULT_OK;
            }
            setResult(state, resultIntent);
            finish();
        }

        if (view.getId() == R.id.checkBox) {
            SharedPreferences.Editor edit = settings.edit();
            edit.putBoolean("remember", remember.isChecked());
            edit.apply();
        }

    }
    /*
    public void error_alert(String alertText){
        AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
        alertDialog.setTitle(R.string.label_err);
        alertDialog.setMessage(alertText);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    */
    public void error_alert(String type, String alertText) {
        AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
        alertDialog.setTitle(type);
        alertDialog.setMessage(alertText);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    Call post(String url, String json, Callback callback) {
        RequestBody body = RequestBody.create(mediaType, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);

        return call;
    }

    private void editSPref(String key, String value){
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

}
