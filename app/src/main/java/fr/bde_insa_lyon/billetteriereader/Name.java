package fr.bde_insa_lyon.billetteriereader;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class Name extends AppCompatActivity implements View.OnClickListener{
    private EditText searchname;
    private String API_URL;
    private String billetvalue;
    private ImageView image;
    private TextView textBillet;
    private RadioButton p1;
    private RadioButton p2;
    private RadioButton p3;
    private RadioButton p4;
    private RadioButton p5;
    protected SharedPreferences settings;


    OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        settings = PreferenceManager.getDefaultSharedPreferences(this);

        API_URL = settings.getString("url", getString(R.string.api_url_prod));

        searchname = findViewById(R.id.editName);
        image = findViewById(R.id.imageView3);
        textBillet = findViewById(R.id.textView3);
        p1 = findViewById(R.id.radioName);
        p2 = findViewById(R.id.radioName1);
        p3 = findViewById(R.id.radioName2);
        p4 = findViewById(R.id.radioName3);
        p5 = findViewById(R.id.radioName4);

        findViewById(R.id.searchBtn).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.searchBtn) {
            //To do : Actions
            String id = searchname.getText().toString();
            String url = API_URL + "/api/search/" + id;

            try {
                String responseGet = run(url);
                JSONArray participants = new JSONArray(responseGet);
                for (int i=0; i < participants.length(); i++){
                    JSONObject participant = participants.getJSONObject(i);
                    String billet = participant.getString("id");
                    String nom = participant.getString("first_name") + " " + participant.getString("last_name");
                    switch (i) {
                        case 0:
                            p1.setText(billet + " - " + nom);
                            break;
                        case 1:
                            p2.setText(billet + " - " + nom);
                            break;
                        case 2:
                            p3.setText(billet + " - " + nom);
                            break;
                        case 3:
                            p4.setText(billet + " - " + nom);
                            break;
                        case 4:
                            p5.setText(billet + " - " + nom);
                            break;
                    }



                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (view.getId() == R.id.back) {
            finish();
        }

    }


    String run(String url) throws IOException {
        String header = "JWT " + settings.getString("token", null);;
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", header)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public void fail_billet(int responsecode, String responsemessage) {
        billetvalue = "Erreur lors de la requête - fail";
        if (responsecode == 200){
            billetvalue = "OK";
        }
        if (responsecode > 399 && responsecode < 500) {
            switch (responsecode){
                case 400:
                    billetvalue = responsemessage;
                    break;
                case 401:
                    billetvalue = getString(R.string.refunded);
                    break;
                case 409:
                    billetvalue = getString(R.string.alreadypassed);
                    break;
                case 406:
                    billetvalue = getString(R.string.nobillet);
                    break;
                case 405:
                    billetvalue = getString(R.string.nofile);
                    break;
                case 423:
                    billetvalue = getString(R.string.notallowedhere);
                    break;
                default:
                    billetvalue = responsemessage;
                    break;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textBillet.setText(billetvalue);
                image.setImageResource(R.mipmap.red_cross);
            }
        });
    }
}
