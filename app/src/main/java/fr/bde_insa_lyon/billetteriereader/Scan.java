package fr.bde_insa_lyon.billetteriereader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Scan extends AppCompatActivity implements View.OnClickListener {
    private CompoundButton autoFocus;
    private CompoundButton useFlash;
    private CompoundButton autoScan;
    private TextView statusMessage;
    private TextView barcodeValue;
    private TextView textBillet;
    private TextView productNo;
    public String billetvalue;
    private String toCast;
    private TextView participantview;
    private ImageView image;
    private String toCastPlace;
    private String part_name;
    private String part_last_name;
    private String opt_name;
    private String opt_amount;
    private ImageView statusCompo ;
    private ImageView statusCheck;
    private boolean optTrue;
    private Switch table;
    private boolean tablesw;
    private boolean dontTouchThis;
    private Button reload;

    private String API_URL;
    public static int QUESTION_ALLERGIES_ID=1; //1 pour la prod, 1 pour le dev /!\ ID HARDCODED !!!! /!\ ಠ_ಠ

    private final MediaType mediaType = MediaType.parse("application/json");
    OkHttpClient client = new OkHttpClient();

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    boolean billetvalide = false;

    protected SharedPreferences settings;

    private HashMap<Integer, String> idToString; //id to Product name

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        settings = PreferenceManager.getDefaultSharedPreferences(this);

        if (settings.getBoolean("devmode", false)) {
            error_alert("Info","DEVMODE active");
        }
        API_URL = settings.getString("url", getString(R.string.api_url_prod));

        statusMessage = (TextView)findViewById(R.id.status_message);
        barcodeValue = (TextView)findViewById(R.id.barcode_value);
        textBillet = (TextView)findViewById(R.id.textBillet);
        participantview = (TextView)findViewById(R.id.textparticipant);
        productNo = (TextView)findViewById(R.id.no_product);
        table = (Switch) findViewById(R.id.switchapero);


        autoFocus = (CompoundButton) findViewById(R.id.auto_focus);
        useFlash = (CompoundButton) findViewById(R.id.use_flash);
        autoScan = (CompoundButton) findViewById(R.id.auto_scan);
        image = (ImageView) findViewById(R.id.imageView2);
        statusCompo = (ImageView) findViewById(R.id.imageComposted);
        statusCheck = (ImageView) findViewById(R.id.imageCheck);
        reload = (Button) findViewById(R.id.refresh);
        optTrue =false;
        billetvalue = null;

        tablesw = table.isChecked();

        findViewById(R.id.read_barcode).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        reload.setOnClickListener(this);

        idToString = new HashMap<Integer, String>();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.read_barcode) {
            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.AutoFocus, autoFocus.isChecked());
            intent.putExtra(BarcodeCaptureActivity.UseFlash, useFlash.isChecked());
            intent.putExtra(BarcodeCaptureActivity.AutoScan, autoScan.isChecked());
            dontTouchThis = false;

            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        }
        if (view.getId() == R.id.back) {
            finish();
        }
        if (view.getId() == R.id.refresh){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.progressBar2).setVisibility(View.VISIBLE);
                    reload.setVisibility(View.GONE);
                    findViewById(R.id.infoToRefresh).setVisibility(View.GONE);
                }
            });
            //proceed to sync get cmochemaisçapaasse
            //get json of products
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            OkHttpClient client = new OkHttpClient();
            String header = "JWT " + settings.getString("token", null);
            Request request = new Request.Builder()
                    .url(settings.getString("url", getString(R.string.api_url_prod)) + "/api/events-products/?event-filter=membership") //prend uniquement les produits pour notre évènement
                    .header("Authorization", header)
                    .build();

            try {
                Log.i("refresh", "recup des produits/options");
                Response response = client.newCall(request).execute();
                int code = response.code();
                try {
                    JSONArray products = new JSONArray(response.body().string());
                    for (int i=0;i<products.length();i++) {
                        JSONObject product = products.getJSONObject(i);
                        //idToString.put(product.getInt("id"), product.getString("name"));
                        Log.i("Product name", product.getString("name"));
                        Log.i("Product id", product.getString("id"));
                        idToString.put(Integer.parseInt(product.getString("id")), product.getString("name"));
                        /*  pas besoin de parse les options, leur nom est déjà indiqué dans le json des billetcheck

                        JSONArray options = product.getJSONArray("options");
                        Log.i("refresh", "produit #"+i+", "+options.length()+" option ");
                        for (int o=0;o<options.length();o++) {
                            JSONObject option = options.getJSONObject(o);
                            String optionName = option.getString("name");
                            Log.i("refresh", optionName);
                            if(optionName.toLowerCase().contains("champagne")) {
                                Log.i("refresh", " -> CHAMPAGNEE détécté!!");
                            }
                            if(optionName.toLowerCase().contains("péniche") || optionName.toLowerCase().contains("peniche")) {
                                Log.i("refresh", " -> (péniche)magnum de champ détécté!!");
                            }
                        }
                        */
                    }
                } catch (JSONException e) {
                    Log.e("refresh", "erreur lors du traitement de "+response.request().url().toString(), e);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("array", idToString.toString());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.progressBar2).setVisibility(View.GONE);
                    findViewById(R.id.read_barcode).setVisibility(View.VISIBLE);
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    statusMessage.setText(R.string.barcode_success);
                    barcodeValue.setText(barcode.displayValue);
                    //Log.d(TAG, "Barcode read: " + barcode.displayValue);
                    testBillet(barcode.displayValue);
                } else {
                    statusMessage.setText(R.string.barcode_failure);
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                statusMessage.setText(String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void testBillet(final String billetBarecode){

        optTrue = false;
        participantview.setText("");
        textBillet.setText("");
        toCast = "";
        tablesw = table.isChecked();

        final String content = "{\n\t\"id\":\""+ billetBarecode +"\"\n}";

        post(API_URL + "/api/billetcheck/", content, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("FAIL");
                System.out.println(e);
                billetvalue = "Erreur lors de la requete - check";
                billetvalide =false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textBillet.setText(billetvalue);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()) {

                    String responseStr = response.body().string();
                    JSONObject reader = null;

                    try {
                        reader = new JSONObject(responseStr);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("Error in JSON read");
                        billetvalue = "Erreur JSON";
                        billetvalide =false;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textBillet.setText(billetvalue);
                            }
                        });
                    }

                    try {

                        String id = reader.getString("id");
                        final String productNumber = reader.getString("product");
                        final JSONArray participants = reader.getJSONArray("participants");

                        toCast = "";

                        boolean toRun = false;

                        for (int i = 0; i < participants.length(); i++) {
                            JSONObject participant = participants.getJSONObject(i);
                            part_name = participant.getString("first_name");
                            part_last_name = participant.getString("last_name");
                            toCast += part_name + " " +part_last_name + "\n";
                            toRun = true;
                        }


                        if (toRun) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String product = idToString.get(Integer.parseInt(productNumber));
                                    productNo.setText(product);
                                    if (product.toLowerCase().contains("je te baise on l'utilise pas cette année")) {
                                        image.setImageResource(R.mipmap.red_tick);
                                        toCast += "- 1 x Magnum Champagne -";
                                        dontTouchThis = true;
                                    }else {
                                        image.setImageResource(R.mipmap.green_tick);
                                    }
                                    statusCheck.setImageResource(R.mipmap.green_tick);
                                    billetvalide = true;
                                    participantview.setText(toCast);
                                }
                            });
                        }

                        JSONArray options = reader.getJSONArray("billet_options");
                        boolean toRun2 = false;
                        optTrue =false;
                        for (int i = 0; i < options.length(); i++) {
                            JSONObject option = options.getJSONObject(i);
                            JSONObject opt = option.getJSONObject("option");
                            opt_name = opt.getString("name");
                            opt_amount = option.getString("amount");
                            toCast += opt_amount + " x " + opt_name + "\n";
                            toRun2 = true;
                            optTrue = true;
                        }

                        if (toRun2) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    participantview.setText(toCast);
                                    billetvalide = true;
                                    image.setImageResource(R.mipmap.red_tick);
                                    statusCheck.setImageResource(R.mipmap.red_tick);
                                }
                            });
                        }

                        if(tablesw) {
                            String url = API_URL + "/api/place/" + id;

                            try{
                                String responseGet = run(url);
                                JSONArray tableArray = new JSONArray(responseGet);
                                toCastPlace = "Nom table - Place \n ";
                                for (int i = 0; i < tableArray.length(); i++) {
                                    JSONObject table = tableArray.getJSONObject(i);
                                    String place = table.getString("place");
                                    JSONObject encoretable = table.getJSONObject("table");
                                    String nom_table = encoretable.getString("nom_table");
                                    toCastPlace += nom_table + " - " + place + "\n";
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        billetvalue = participantview.getText().toString();
                                        participantview.setText(toCastPlace); //BUG ICI ou pas :)
                                        image.setImageResource(R.mipmap.green_tick);
                                        statusCompo.setImageResource(R.mipmap.green_tick);
                                    }
                                });
                            }catch (IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        image.setImageResource(R.mipmap.red_cross);
                                        statusCompo.setImageResource(R.mipmap.red_cross);
                                        textBillet.setText("Erreur lors de l'attribution des places");
                                    }
                                });
                            }

     //update Jean

                            try { //récup des allergies
                                String responseQuestion = run(API_URL + "/api/admin/answers/?question="+QUESTION_ALLERGIES_ID+"+&billet=" + id);
                                JSONArray answerArray = new JSONArray(responseQuestion); //en réalité pour les réponses aux allergies il n'y aura jamais qu'une réponse
                                String reponseAllergie = "";
                                for(int i=0; i<answerArray.length();i++) {
                                    JSONObject answerJson = answerArray.getJSONObject(i);
                                    if (!answerJson.getString("value").equals("null")) {
                                        reponseAllergie += answerJson.getString("value");
                                    }
                                }
                                Log.i("allergies", reponseAllergie);
                                if(reponseAllergie.length()>1) {
                                    toCastPlace+= "\n" + reponseAllergie;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            participantview.setText(toCastPlace);
                                            statusCompo.setImageResource(R.drawable.ic_details_black_24dp);
                                            image.setImageResource(R.drawable.ic_details_black_24dp);
                                        }
                                    });
                                }
                            } catch (Exception e) { //yess de la bonne programmation
                                Log.e("allergies", "erreur lors de la récup", e);
                            }

     //fin jean

                        } else {
                            String content = "{\n\t\"billet\":\"" + id + "\",\n\t\"file\":\"" + settings.getInt("file", 1) + "\"\n}";
                            post(API_URL + "/api/compostages/", content, new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    System.out.println("FAIL COMPOSTAGE");
                                    System.out.println(e);
                                    billetvalue = "Erreur call compostage";
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            statusCompo.setImageResource(R.mipmap.red_cross);
                                            image.setImageResource(R.mipmap.red_cross);
                                        }
                                    });
                                }

                                @Override
                                public void onResponse(Call call, Response response) throws IOException {
                                    if (response.isSuccessful() && !optTrue) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (dontTouchThis){

                                                }else {
                                                    image.setImageResource(R.mipmap.green_tick);
                                                }
                                                statusCompo.setImageResource(R.mipmap.green_tick);

                                            }
                                        });
                                    } else if (response.isSuccessful() && optTrue) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                statusCompo.setImageResource(R.mipmap.green_tick);
                                                image.setImageResource(R.mipmap.red_tick);
                                            }
                                        });
                                    } else {
                                        fail_billet(response.code(), response.message());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                statusCompo.setImageResource(R.mipmap.red_cross);
                                                image.setImageResource(R.mipmap.red_cross);
                                            }
                                        });
                                    }

                                }
                            });
                        }

                        if (billetvalide && !tablesw) {
                            billetvalue = "Ticket valide - Billet n° " + id;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        billetvalue = "Ticket non valide";
                        billetvalide =false;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textBillet.setText(billetvalue);
                            }
                        });
                    }

                } else {
                    // Request not successful
                    billetvalide = false;
                    fail_billet(response.code(), response.message());
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textBillet.setText(billetvalue);
                    }
                });

            }

        }); //fin post
    } //fin billetcheck

    Call post(String url, String json,Callback callback) {
        String header = "JWT " + settings.getString("token", null);
        RequestBody body = RequestBody.create(mediaType, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Authorization", header)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);

        return call;
    }

    String run(String url) throws IOException {
        String header = "JWT " + settings.getString("token", null);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", header)
                .build();
        Log.d("httpCall", request.url().toString());
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public void error_alert(String alertText){
        AlertDialog alertDialog = new AlertDialog.Builder(Scan.this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage(alertText);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void error_alert(String type, String alertText) {
        AlertDialog alertDialog = new AlertDialog.Builder(Scan.this).create();
        alertDialog.setTitle(type);
        alertDialog.setMessage(alertText);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void fail_billet(int responsecode, String responsemessage) {
         billetvalue = "Erreur lors de la requête - fail";
         if (responsecode == 200){
             billetvalue = "OK";
         }
         if (responsecode > 399 && responsecode < 500) {
            switch (responsecode){
                case 400:
                    billetvalue = responsemessage;
                    break;
                case 401:
                    billetvalue = getString(R.string.refunded);
                    break;
                case 409:
                    billetvalue = getString(R.string.alreadypassed);
                    break;
                case 406:
                    billetvalue = getString(R.string.nobillet);
                    break;
                case 405:
                    billetvalue = getString(R.string.nofile);
                    break;
                case 423:
                    billetvalue = getString(R.string.notallowedhere);
                    break;
                default:
                    billetvalue = responsemessage;
                    break;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textBillet.setText(billetvalue);
                image.setImageResource(R.mipmap.red_cross);
                if (!billetvalide){
                    statusCheck.setImageResource(R.mipmap.red_cross);
                    statusCompo.setImageResource(R.mipmap.red_cross);
                }
            }
        });
    }
}
