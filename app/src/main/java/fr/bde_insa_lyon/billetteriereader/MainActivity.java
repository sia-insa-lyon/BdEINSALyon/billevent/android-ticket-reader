package fr.bde_insa_lyon.billetteriereader;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int LOGIN_STATE = 1024;
    private static final int RC_BARCODE_CAPTURE = 9001;
    public String LOGIN_ERR;
    public boolean login_state;
    private static CompoundButton devmode;
    private TextView tocken;
    protected SharedPreferences settings;
    private static final int RC_INTERNET_PERMISSION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] permissions = new String[]{Manifest.permission.INTERNET};

        if(!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {
            ActivityCompat.requestPermissions(this, permissions, RC_INTERNET_PERMISSION);
            // if (MainActivity.DEVMODE) error_alert("Internet prompt");
        }

        settings = PreferenceManager.getDefaultSharedPreferences(this);

        LOGIN_ERR = getString(R.string.login_err);

        devmode = (CompoundButton) findViewById(R.id.dev_mode);
        tocken = (TextView) findViewById(R.id.textToken);

        String token =  null;
        if (settings.contains("token")) {
            token = settings.getString("token", null);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            OkHttpClient client = new OkHttpClient();
            String header = "JWT " + settings.getString("token", null);
            Request request = new Request.Builder()
                    .url(settings.getString("url", getString(R.string.api_url_prod)) + "/api/events/")
                    .header("Authorization", header)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                int code = response.code();
                if (code != 200) token = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (token == null){
            tocken.setText(R.string.not_connected);
        }else{
            tocken.setText(token);
            login_state = true;
        }

        devmode.setChecked(settings.getBoolean("devmode", false));


        findViewById(R.id.loginBtn).setOnClickListener(this);
        findViewById(R.id.scanBtn).setOnClickListener(this);
        //findViewById(R.id.nameBtn).setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        SharedPreferences.Editor edit = settings.edit();
        edit.putBoolean("devmode", MainActivity.devmode.isChecked());
        if (MainActivity.devmode.isChecked()) {
            edit.putString("url", getString(R.string.api_url));
        }else {
            edit.putString("url", getString(R.string.api_url_prod));
        }
        edit.apply();
        if (view.getId() == R.id.loginBtn) {
            Intent intend = new Intent(this, Login.class);
            startActivityForResult(intend, LOGIN_STATE);
        }
        if (view.getId() == R.id.scanBtn) {
            if (!login_state){
                error_alert(LOGIN_ERR);
                return;
            }
            Intent intend = new Intent(this, Scan.class);
            startActivity(intend);
        }
        if (view.getId() == R.id.nameBtn) {
            if (!login_state){
                error_alert(LOGIN_ERR);
                return;
            }
            Intent intend = new Intent(this, Name.class);
            startActivity(intend);
        }

    }

    public void error_alert(String alertText){
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.label_err));
        alertDialog.setMessage(alertText);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_STATE){
            if (resultCode == Activity.RESULT_OK){
                login_state = true;
                tocken.setText(settings.getString("token", null));
            }
        }
    }
}
